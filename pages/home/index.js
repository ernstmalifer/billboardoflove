/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import Layout from '../../components/Layout';
import s from './styles.css';

import { ViewPager, Frame, Track, View, AnimatedView } from 'react-view-pager';

import 'whatwg-fetch';

import _ from 'lodash';
import { extend } from 'lodash/extend';

class HomePage extends React.Component {

  static propTypes = {
  };

  constructor(props) {
    super(props);

    this.state = {
      currentLoversID: 0,
      emojiInterval: 3000,
      smsInterval: 5000,
      loverInterval: 15000,
      lovers: [],
      played: []
    };

  } 

  changeEmoji(){
    this.refs.tracker.next();
  }

  changeSMS(){
    this.refs.tracker2.next();
  }

  // OLD ALGO
  // getLovers = (init) => {

  //   let parent = this;

  //   // fetch('https://admin.cosi.rawideas.com/index.php/cosi/datafeed/TLifeValentinesFeed_60226afe0adbf6e8775e1bb4fa112a33/desc/250', { mode: 'no-cors' })
  //   fetch('http://proxy.engagisdemo.com.au/index.php?csurl=https://admin.cosi.rawideas.com/index.php/cosi/datafeed/TLifeValentinesFeed_60226afe0adbf6e8775e1bb4fa112a33/desc/250')
  //   .then(function(response) {
  //     return response.json()
  //   }).then(function(json) {
  //     console.log(json);
  //     parent.setState({'lovers': json});


  //     if(init) {
  //       parent.setState({'currentLoversID': parent.state.lovers[0].id});
  //     } else {
  //       if(parent.state.currentLoversID == json[0].id){

  //         var randomLovers;

  //         do {
  //           randomLovers = json[Math.floor(Math.random() * json.length)];
  //         } while(parent.state.currentLoversID == randomLovers.id)
  //         parent.setState({'currentLoversID': randomLovers.id});

  //       } else {
  //         parent.setState({'currentLoversID': json[0].id});
  //       }
  //     }

  //   }).catch(function(ex) {
  //     console.log('parsing failed', ex)
  //   })
  // }

  getLovers = (init) => {

    let parent = this;

    // fetch('https://admin.cosi.rawideas.com/index.php/cosi/datafeed/TLifeValentinesFeed_60226afe0adbf6e8775e1bb4fa112a33/desc/250', { mode: 'no-cors' })
    fetch('http://proxy.engagisdemo.com.au/index.php?csurl=https://admin.cosi.rawideas.com/index.php/cosi/datafeed/TLifeValentinesFeed_60226afe0adbf6e8775e1bb4fa112a33/desc/250')
    .then(function(response) {
      return response.json()
    }).then(function(json) {
      parent.setState({'lovers': json });

      for (var i = parent.state.lovers.length; i-- >= 0; ){
        try{
          if(parent.state.played.indexOf( parent.state.lovers[i].id ) == -1){
            parent.state.played.push(parent.state.lovers[i].id);
            parent.setState({'currentLoversID': parent.state.lovers[i].id});
            break;
          }
        } catch(e){
          var randomLovers = json[Math.floor(Math.random() * json.length)];
          parent.setState({'currentLoversID': randomLovers.id});
        }
      }


    }).catch(function(ex) {
      console.log('parsing failed', ex)
    })
  }

  componentWillMount() {

    document.title = 'Billboard of Love';
    this.getLovers(1);

  }

  componentDidMount() {
    this.changeEmojiInterval = setInterval(() => this.changeEmoji(), this.state.emojiInterval);
    this.getSMSInterval = setInterval(() => this.changeSMS(), this.state.smsInterval);

    this.getLoversInterval = setInterval(() => this.getLovers(), this.state.loverInterval);
  }

  componentWillUnmount(){
    clearInterval(this.changeEmojiInterval);
    clearInterval(this.getLoversInterval);
  }

  render() {

    var loverOne = '';
    var loverTwo = '';
    var lovers = this.state.lovers.find(lover => lover.id == this.state.currentLoversID);
    if(lovers){
      loverOne = lovers.field2;
      loverTwo = lovers.field3;
    }

    return (
      <Layout className={s.content}>
        <div className={s.main}>
          <div className={s.info}>
            <strong>SMS</strong><br />
            <ViewPager>
            <Frame className={s.frame2}>
            <Track ref="tracker2" viewsToShow={1} axis="y" infinite className={s.track2}>
              <View>
                <span>0499</span><br />
                <span>695 683</span>
              </View>
              <View>
                <span>0499</span><br />
                <span>MY LOVE</span>
              </View>
              </Track>
            </Frame>
        </ViewPager>
          </div>
          <div className={s.lovers} style={{marginLeft: '0px'}}>
            <div className={s.loverOne}>{loverOne}</div>
            <div className={s.emoji}>

              <ViewPager>
          <Frame className="frame">
            <Track ref="tracker" viewsToShow={1} axis="y" infinite className="track">
              <View className="view"><img src="images/emojis/heart_twighlight.png" /></View>
              <View className="view"><img src="images/emojis/heart_eyes_5.png" /></View>
              <View className="view"><img src="images/emojis/heart_pulse_13.png" /></View>
              <View className="view"><img src="images/emojis/two_hearts_4.png" /></View>
              {/* <View className="view"><img src="images/emojis/kissing_heart 4.png" /></View>
              <View className="view"><img src="images/emojis/heart_sunrise.png" /></View>
              <View className="view"><img src="images/emojis/heart_pacific.png" /></View>
              <View className="view"><img src="images/emojis/heart_rockpool.png" /></View> */}
            </Track>
          </Frame>
        </ViewPager>

            </div>
            <div className={s.loverTwo}>{loverTwo}</div>
          </div>

          {/**/
          <div className={s.hearts}>
            <div className={s.heart} style={{transform: `rotate(15deg)`, top: '15px', left: '8px'}}>
              <div className={s.heartInner + " " + s.pulse} style={{backgroundImage: `url("images/emojis/heart_rockpool.png")`, animationDelay: '0s'}}></div>
            </div>
            <div className={s.heart} style={{transform: `rotate(-5deg)`, top: '63px', left: '92px'}}>
              <div className={s.heartInner + " " + s.pulse} style={{backgroundImage: `url("images/emojis/heart_pacific.png")`, animationDelay: '.3s'}}></div>
            </div>
            <div className={s.heart} style={{transform: `rotate(-10deg)`, top: '123px', left: '30px'}}>
              <div className={s.heartInner + " " + s.pulse} style={{backgroundImage: `url("images/emojis/heart_sunrise.png")`, animationDelay: '.6s'}}></div>
            </div>
            <div className={s.heart} style={{transform: `rotate(0deg)`, top: '140px', left: '136px'}}>
              <div className={s.heartInner + " " + s.pulse} style={{backgroundImage: `url("images/emojis/heart_twighlight.png")`, animationDelay: '.9s'}}></div>
            </div>
          </div>
        /**/}

        </div>
      </Layout>
    );
  }

}

export default HomePage;